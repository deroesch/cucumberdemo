package org.deroesch.cucumber_demo_01;

import java.util.LinkedList;
import java.util.List;

public class Stack {

	List<String> stack = new LinkedList<String>();

	public Stack() {
		super();
	}

	public void push(String s) {
		stack.add(s);
	}

	public String pop() {
		return stack.isEmpty() ? null : stack.remove(stack.size() - 1);
	}

	public String top() {
		return stack.isEmpty() ? null : stack.get(stack.size() - 1);
	}

	public int size() {
		return stack.size();
	}

	public void clear() {
		stack.clear();
	}

}
