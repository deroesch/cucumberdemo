/**
 * 
 */
package org.deroesch.cucumber_demo_01;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * @author CORP\deroeschu
 *
 */
class StackTest {

	Stack stack;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
		stack = new Stack();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link org.deroesch.cucumber_demo_01.Stack#Stack()}.
	 */
	@Test
	void testStack() {
		assertEquals(0, stack.size());
	}

	/**
	 * Test method for
	 * {@link org.deroesch.cucumber_demo_01.Stack#push(java.lang.String)}.
	 */
	@Test
	void testPush() {
		assertEquals(0, stack.size());
		stack.push("a");
		assertEquals(1, stack.size());
		stack.push("b");
		assertEquals(2, stack.size());
	}

	/**
	 * Test method for {@link org.deroesch.cucumber_demo_01.Stack#pop()}.
	 */
	@Test
	void testPop() {
		assertNull(stack.pop());
		stack.push("Hello");
		assertEquals("Hello", stack.pop());
		assertEquals(0, stack.size());
	}

	/**
	 * Test method for {@link org.deroesch.cucumber_demo_01.Stack#top()}.
	 */
	@Test
	void testTop() {
		assertNull(stack.top());
		stack.push("Hello");
		assertEquals("Hello", stack.top());
		assertEquals(1, stack.size());
	}

	/**
	 * Test method for {@link org.deroesch.cucumber_demo_01.Stack#size()}.
	 */
	@Test
	void testSize() {
		assertEquals(0, stack.size());
		stack.push("a");
		assertEquals(1, stack.size());
	}

	/**
	 * Test method for {@link org.deroesch.cucumber_demo_01.Stack#clear()}.
	 */
	@Test
	void testClear() {
		assertEquals(0, stack.size());
		stack.push("a");
		stack.push("b");
		stack.push("c");
		assertEquals(3, stack.size());
		stack.clear();
		assertEquals(0, stack.size());
	}

}
